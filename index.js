let config = require('./libs/config');
let fn = require('./libs/fn');
let express = require('express');
let app = express();
let server = require('http').Server(app);
let io = require('socket.io')(server);
let get = require('./libs/get');
let sockets = require('./libs/sockets');


server.listen(config.get('port'), function () {
    console.log('listening on *:' + config.get('port'));
});

io.on('connection', sockets.handler);
