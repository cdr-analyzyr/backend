
Date.prototype.daysInMonth = function () {
    return 32 - new Date(this.getFullYear(), this.getMonth(), 32).getDate();
};
function addZero(digits_length, source) {
    "use strict"
    let text = source + '';
    while (text.length < digits_length)
        text = '0' + text;
    return text;
}
module.exports.monthEnd = function () {
    let date = new Date();
    let dateStr = date.getFullYear() + "-" + addZero(2, (date.getMonth() + 1)) + "-" + addZero(2, new Date().daysInMonth());
    return dateStr;
}

module.exports.monthStart = function () {
    let date = new Date();
    let dateStr = date.getFullYear() + "-" + addZero(2, (date.getMonth() + 1)) + "-" + "01";
    return dateStr;
}