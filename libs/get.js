let config = require('./config');
let mysql = require('mysql');
let connection = mysql.createConnection(config.get('mysql'));

module.exports.fullStats = function (dataset, socket) {

    let socketEmit = dataset.socketEmit || "fullStats";
    let forSend = dataset;
    let ourPhones = config.get('lines');
    let start = createStart(dataset);
    let end = createEnd(dataset);

    let has_recall_sql = config.get('sql:has_recall')
        .replace(/###START###/g, start)
        .replace(/###END###/g, end)
        .replace(/###OUR_PHONES###/g, ourPhones);
    let answered_calls_sql = config.get('sql:answered_calls')
        .replace(/###START###/g, start)
        .replace(/###END###/g, end)
        .replace(/###OUR_PHONES###/g, ourPhones);
    let all_unq_cids_sql = config.get('sql:all_unq_cids')
        .replace(/###START###/g, start)
        .replace(/###END###/g, end)
        .replace(/###OUR_PHONES###/g, ourPhones);
    let not_answered_calls_sql = config.get('sql:not_answered_calls')
        .replace(/###START###/g, start)
        .replace(/###END###/g, end)
        .replace(/###OUR_PHONES###/g, ourPhones);

    console.log('\n', has_recall_sql)
    console.log('\n', answered_calls_sql)
    console.log('\n', all_unq_cids_sql)
    console.log('\n', not_answered_calls_sql)

    connection.query(all_unq_cids_sql, function (err, results, fields) {
        forSend.unq_cids = results[0].all_incoming_num;
        connection.query(has_recall_sql, function (err, results, fields) {
            forSend.has_recall = results[0].has_recall;
            connection.query(answered_calls_sql, function (err, results, fields) {
                forSend.answered_calls = results[0].answered_calls;
                connection.query(not_answered_calls_sql, function (err, results, fields) {
                    forSend.not_answered_calls = results[0].not_answered_calls;
                    socket.emit(socketEmit, forSend);
                })
            })
        })
    })


};

module.exports.list = function (dataset, socket) {
    let forSend = dataset;
    let ourPhones = config.get('lines');
    let start = createStart(dataset);
    let end = createEnd(dataset);
    let type = dataset.type || "not_answered_calls_list";
    console.log(type);
    let sql = config.get('sql:' + type)
        .replace(/###START###/g, start)
        .replace(/###END###/g, end)
        .replace(/###OUR_PHONES###/g, ourPhones)
        // .replace(/SELECT COUNT\(/g, "SELECT ")
        // .replace(/\)\) as/g, ") as");
    console.log(sql);
    connection.query(sql, function (err, results, fields) {
        forSend.results = results;
        socket.emit('list', forSend);
    })

}

module.exports.aboutPhone = function (dataset, socket) {
    let forSend = dataset;
    let ourPhones = config.get('lines');
    let start = createStart(dataset);
    let end = createEnd(dataset);
    let phone = dataset.phone || false;
    if (phone) {
        let sql = config.get('sql:aboutPhone')
            .replace(/###NUMBER###/g, phone);
        connection.query(sql, function (err, results, fields) {
            forSend.results = results;
            socket.emit('aboutPhone', forSend);
        })
    }

};

let createEnd = (dataset) => {
    let date = dataset.dateEnd || "2050-01-01";
    let time = dataset.timeEnd || "00:00:00";
    let end = date + " " + time;
    return end;
}

let createStart = (dataset) => {
    let dateStart = dataset.dateStart || "1970-01-01";
    let timeStart = dataset.timeStart || "00:00:00"
    let start = dateStart + " " + timeStart;
    return start;
}