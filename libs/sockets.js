let config = require('./config');
let fn = require('./fn');
let get = require('./get');

module.exports.handler = function (socket) {
    socket.on('fullStats',function (dataset) {
        get.fullStats(dataset,socket);
    })
    socket.on('fullStatsMonth',function (dataset) {
        dataset.dateStart = fn.monthStart();
        dataset.dateEnd = fn.monthEnd();
        dataset.socketEmit = 'fullStatsMonth';
        get.fullStats(dataset,socket);
    })
    socket.on('getList',function (dataset) {
        console.log('getList');
        get.list(dataset, socket);
        get.fullStats(dataset, socket);
    })
    socket.on('getAboutPhone',function (dataset) {
        get.aboutPhone(dataset, socket);
    })
}